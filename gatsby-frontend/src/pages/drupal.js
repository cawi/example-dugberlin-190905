import React from "react";
import {Link} from "gatsby";

import Layout from "../components/layout";
import SEO from "../components/seo";
import {useStaticQuery, graphql} from "gatsby";

const DrupalPage = () => {
  const data = useStaticQuery(graphql`query drupalQuery {
    allNodeEvent {
      edges {
        node {
          id
          title
          field_date(formatString: "dd, DD.MM.YYYY - HH:mm", locale: "de", fromNow: true)
          field_description {
            value
          }
        }
      }
    }
  }
  `);
  return (
    <Layout>
      <SEO title="Home" />
      <h1>Hi people</h1>
      <p>These are our next events</p>
      {data.allNodeEvent.edges.map(({node}) => { return(
        <div key={node.id}>
          <h2 style={{marginBottom: '.5rem'}}>{node.title}</h2>
          <p><small>{node.field_date}</small></p>
          <div style={{paddingBottom: '1rem'}} dangerouslySetInnerHTML={{__html:node.field_description.value}}/>
        </div>
        )
      })}
      <Link to="/">Go to start</Link>
    </Layout>
  )
}


export default DrupalPage
