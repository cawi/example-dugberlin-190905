# Drupal 8 decoupled project
This Drupal project is build with composer. The Frontend is create with static site generator GATSBY.


## File Structure
    
    root
    |-- config
    |-- console
    |-- database
    |-- drush
    |-- gatsby-frontend
    |-- scripts
    |-- vendor
    |-- web


## Installation Drupal 8

`composer install`



## Database
You find the database in the database-directory. Check the file _setting.php_ and change according your needs.


## Passwords
Password for access in Drupal backend:

name: admin

pw: admin


## Backend
There is a custom backend for editors and authors -> Verwaltung Café. You find it in the toolbar


## Frontend Installation
To make the Gatsby frontend run 

`cd gatsby-frontend`

 `npm install`

`gatsby develop`

## Note
For the ones who do not want to install but want to see what Gatsby generate I pushed the gatsby-frontend/public-directory, too. Normaly it is not necessary.
